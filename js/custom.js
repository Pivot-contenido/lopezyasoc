$('#derecho-tributario').click(function () {
   $('.derecho-tributario').addClass('opacity');
});
$('#derecho-tributario-v').click(function () {
   $('.derecho-tributario').removeClass('opacity');
});


$('#derecho-laboral').click(function () {
   $('.derecho-laboral').addClass('opacity');
});
$('#derecho-laboral-v').click(function () {
   $('.derecho-laboral').removeClass('opacity');
});

$('#derecho-societario').click(function () {
   $('.derecho-societario').addClass('opacity');
});
$('#derecho-societario-v').click(function () {
   $('.derecho-societario').removeClass('opacity');
});

$('#procesos').click(function () {
   $('.procesos').addClass('opacity');
});
$('#procesos-v').click(function () {
   $('.procesos').removeClass('opacity');
});

$('#impositivo').click(function () {
   $('.impositivo').addClass('opacity');
});
$('#impositivo-v').click(function () {
   $('.impositivo').removeClass('opacity');
});

$('#finanzas').click(function () {
   $('.finanzas').addClass('opacity');
});
$('#finanzas-v').click(function () {
   $('.finanzas').removeClass('opacity');
});

$('#transferencia').click(function () {
   $('.transferencia').addClass('opacity');
});
$('#transferencia-v').click(function () {
   $('.transferencia').removeClass('opacity');
});


$('.nav-item').click(function () {
   $('.nav-item.active').removeClass('active');
   $(this).addClass('active');
});

$(document).on("click", ".nav-item", function () {
   jQuery(".nav-item").closest(".bsnav-mobile").removeClass("in");
   jQuery(".toggler-spin").removeClass("active");
});

$(function () {
   $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      if (scroll >= 150) {
         $(".menu-overlay ").addClass("menu-in menu-shadow");
      } else {
         $(".menu-overlay").removeClass("menu-in menu-shadow");
      }
   });
});

$(document).scroll(function () {
   if ($(window).scrollTop() === 0) {
      $('.nav-item.active').removeClass('active');
   }
});
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
   anchor.addEventListener('click', function (e) {
      e.preventDefault();

      document.querySelector(this.getAttribute('href')).scrollIntoView({
         behavior: 'smooth'
      });
   });
});
AOS.init();
